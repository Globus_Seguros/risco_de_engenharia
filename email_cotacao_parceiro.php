<?php
require_once ('mail.php');
require_once('globals.php');


function envia_email_cotacao_parceiro ($obj, $email_parceiro)
{
    ///$obj = json_decode($json, false);
    $file = file_get_contents('./email_notificacao_parceiro.html', FILE_USE_INCLUDE_PATH);
    date_default_timezone_set('America/Sao_Paulo');


    $timestamp = date('d/m/Y G:i:s');
    $file = str_replace("|#timestamp#|", $timestamp, $file);


    $file = str_replace("|#nome#|", $obj->{'nome'}, $file);

    $file = str_replace("|#tipo_pessoa#|", $obj->{'tipo_pessoa'}, $file);
    if ( $obj->{"tipo_pessoa"} == "Fisica") {
        $file = str_replace("|#cpf#|", "CPF: " . $obj->{'cpf'}, $file);
    }
    else {
        $file = str_replace("|#cpf#|", "CNPJ: " . $obj->{'cnpj'}, $file);
    }

    $file = str_replace("|#email#|", $obj->{'email'}, $file);
    $file = str_replace("|#telefone#|", $obj->{'telefone'}, $file);
    $file = str_replace("|#prazoMes#|", $obj->{'prazoMes'}, $file);
    $file = str_replace("|#prazoDia#|", $obj->{'prazoDia'}, $file);
    $file = str_replace("|#valor#|", $obj->{'valor'}, $file);
    $file = str_replace("|#endereco#|", $obj->{'endereco'}, $file);
    $file = str_replace("|#numero#|", $obj->{'numero'}, $file);
    $file = str_replace("|#complemento#|", $obj->{'complemento'}, $file);
    $file = str_replace("|#cep#|", $obj->{'cep'}, $file);
    $file = str_replace("|#bairro#|", $obj->{'bairro'}, $file);
    $file = str_replace("|#cidade#|", $obj->{'cidade'}, $file);
    $file = str_replace("|#uf#|", $obj->{'uf'}, $file);
    $file = str_replace("|#reforma#|", $obj->{'reforma'}, $file);
    $file = str_replace("|#obs#|", $obj->{'obs'}, $file);
    $file = str_replace("|#estrutural#|", $obj->{'estrutural'}, $file);
    $file = str_replace("|#demolicao#|", $obj->{'demolicao'}, $file);
    $file = str_replace("|#pavimentos#|", $obj->{'pavimentos'}, $file);
    $file = str_replace("|#subsolos#|", $obj->{'subsolos'}, $file);


    $body = $file;
   // echo $body;

    if($GLOBALS['TESTE']) {
        sendEMail('vinicius.souza@globusseguros.com.br', $body, 'Teste - Nova solicitação de contato');
    }
    else{
        sendEMail($email_parceiro, $body,'', 'Nova solicitação de contato');
    }
}

?>
