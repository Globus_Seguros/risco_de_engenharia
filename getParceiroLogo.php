<?php
require_once('globals.php');
try {
    $id_parceiro = stripslashes($_REQUEST['id']);
    if(!isset($id_parceiro) || !$id_parceiro){
        $response['status'] = array(
            'type' => 'error',
            'value' => 'wrong id',
        );
        exit($response);
    }
    if($GLOBALS['TESTE']){
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=rcmedicoTestdb user=RevoluTISys password=globusSeguros0");
    }
    else {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=globusdb user=RevoluTISys password=globusSeguros0");
        console.log(">>>>>>>>>>>>>>>>>Erro");
    }
    $query = "SELECT link_logo as logo, bg_color_logo as bgcolor FROM rede_parceiro LEFT JOIN rede_parceiro_produto on rede_parceiro.id_parceiro=rede_parceiro_produto.id_parceiro WHERE rede_parceiro.id_parceiro='" . $id_parceiro . "' AND id_produto='Risco de Engenharia' AND ativo=true;";


    $result = pg_query($bdcon, $query);
    if (!$result) {
        $response['status'] = array(
            'type' => 'error',
            'value' => 'no result found',
        );
        exit($response);
    }
    else
        $array = pg_fetch_all($result);
    exit(json_encode($array));
}catch(Exception $e){
    $response['status'] = array(
        'type' => 'error',
        'value' => 'exception thrown',
    );
    exit($response);
}
