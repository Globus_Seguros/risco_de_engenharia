<?php
require_once('./../globals.php');
require_once('../util.php');
require_once('SendMail.php');

class Mail
{
    public $sendMail;

    public function __construct()
    {
        $this->sendMail = new SendMail();
    }

    public function envia_email_cotacao_cliente($obj)
    {
        $file = file_get_contents('./email_notificacao_cliente.html', FILE_USE_INCLUDE_PATH);
        date_default_timezone_set('America/Sao_Paulo');

        $timestamp = date('d/m/Y G:i:s');
        $file = str_replace("|#timestamp#|", $timestamp, $file);
//        $file = str_replace("|#id_cotacao#|", $id_cotacao, $file);

        $file = str_replace("|#nome#|", $obj->{'nome'}, $file);
        $file = str_replace("|#email#|", $obj->{'email'}, $file);
        $file = str_replace("|#celular#|", $obj->{'celular'}, $file);
        $file = str_replace("|#endereco#|", $obj->{'endereco'}, $file);
        $file = str_replace("|#nascimento#|", $obj->{'nascimento'}, $file);

        $body = $file;

        if ($GLOBALS['TESTE']) {
//            sendEMail($obj->{'email'}, '', $body, 'Teste - Seu contato - Seguro Odontológico');
            $this->sendMail->sendEMail($obj->{'email'}, '', $body, 'Teste - Seu contato - Seguro Odontológico');
        } else {
//            sendEMail($obj->{'email'}, '', $body, 'Seu contato - Seguro Odontológico');
            $this->sendMail->sendEMail($obj->{'email'}, '', $body, 'Seu contato - Seguro Odontológico');
        }
    }


    public function envia_email_para_cotacao($obj)
    {
        ///$obj = json_decode($json, false);
        $file = file_get_contents('./email_notificacao_cotacao.html', FILE_USE_INCLUDE_PATH);
        date_default_timezone_set('America/Sao_Paulo');

        $parceiro = $obj->{'parceiro'};
        if (!$parceiro) {
            $parceiro = $GLOBALS['Parceiro_padrao'];
        }
        $timestamp = date('d/m/Y G:i:s');
        $file = str_replace("|#timestamp#|", $timestamp, $file);
//    $file = str_replace("|#id_cotacao#|", $id_cotacao, $file);
//    $file = str_replace("|#id_parceiro#|", $id_parceiro, $file);

//    if (isset($nome_corretor)) {
//        $file = str_replace("|#nome_corretor#|", $nome_corretor, $file);
//        $file = str_replace("|#email_corretor#|", $email_corretor, $file);
//    } else {
//        $file = str_replace("|#nome_corretor#|", "Não informado", $file);
//        $file = str_replace("|#email_corretor#|", "Não informado", $file);
//    }

        $file = str_replace("|#nome#|", $obj->{'nome'}, $file);
        $file = str_replace("|#email#|", $obj->{'email'}, $file);
        $file = str_replace("|#celular#|", $obj->{'celular'}, $file);
        $file = str_replace("|#endereco#|", $obj->{'endereco'}, $file);
        $file = str_replace("|#nascimento#|", $obj->{'nascimento'}, $file);

        $body = $file;
        // echo $body;

        //    $start = strpos($body, "<BR>");
        //    $end = strrpos($body, "<BR>");
        //    $body2 = substr($body, $start + 4, ($end - $start) - 4);
        //    $body2 = str_replace("'", '"', $body2);

        $resp_comercial = email_gestor_comercial($obj->{'id_parceio'});

        $arrayEmails = [$resp_comercial[0]['email_usuario']];


        if ($GLOBALS['TESTE']) {
            $this->sendMail->sendEMail('jefferson.martins@globusseguros.com.br', $arrayEmails, $body, 'Teste - Nova solicitação de contato');

        } else {
//            $this->sendMail->sendEMail('cotacao@globusseguros.com.br', $arrayEmails, $body, 'Nova solicitação de contato');
        }
    }

}