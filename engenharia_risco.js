$(document).ready(function () {

    $(document).ready(function () {
        $('select').formSelect();
    });

    $.ajax
    ({
        type: "GET",
        url: "getParceiroLogo.php",
        dataType: 'json',
        data: "id=" + getUrlParameter('pc'),
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            console.log(result);
            if (result && result[0].logo && result[0].bgcolor) {
                $('#logo').attr('src', result[0].logo);
                $('#templateHeader').css("background-color", result[0].bgcolor);
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log("Status: " + textStatus + " Error: " + errorThrown);
        },
        complete: function (data) {
            $("#templateHeader").removeClass("hide");
        }
    });

    $("#fisica").hide();
    $("#juridica").hide();



    $("#tipo_pessoa").change(function () {
        var i = $("#tipo_pessoa").val();
        //console.log("Entro com i=", i);
        if (i == "Fisica") {
            $('#fisica').show('slow');
            $('#juridica').hide('slow');
        } else if (i == "Juridica") {
            $('#juridica').show('slow');
            $('#fisica').hide('slow');
        }
    });

    function getScripts(scripts, callback) {
        var progress = 0;
        scripts.forEach(function (script) {
            $.getScript(script, function () {
                if (++progress == scripts.length) callback();
            });
        });
    }


    getScripts(['./mask.js'], function () {

        $('#telefone').mask('(00) 00000-0000');
        $('#cnpj').mask('00.000.000/0000-00');
        $('#prazoMes').mask('00/00/0000');
        $('#prazoDia').mask('00/00/0000');
        $('#cep').mask('00000-000');
        $('#cpf').mask('000.000.000-00');

    });

});


    function moeda(a, e, r, t) {
        let n = ""
            , h = j = 0
            , u = tamanho2 = 0
            , l = ajd2 = ""
            , o = window.Event ? t.which : t.keyCode;

        if (13 == o || 8 == o)
            return !0;
        if (n = String.fromCharCode(o),
        -1 == "0123456789".indexOf(n))
            return !1;
        for (u = a.value.length,
                 h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
            ;
        for (l = ""; h < u; h++)
            -1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
        if (l += n,
        0 == (u = l.length) && (a.value = ""),
        1 == u && (a.value = "0" + r + "0" + l),
        2 == u && (a.value = "0" + r + l),
        u > 2) {
            for (ajd2 = "",
                     j = 0,
                     h = u - 3; h >= 0; h--)
                3 == j && (ajd2 += e,
                    j = 0),
                    ajd2 += l.charAt(h),
                    j++;
            for (a.value = "R$",
                     tamanho2 = ajd2.length,
                     h = tamanho2 - 1; h >= 0; h--)
                a.value += ajd2.charAt(h);
            a.value += r + l.substr(u - 2, u)

        }
        return  !1
    }

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }

}

function valida_form() {


    var nome = $('#nome').val();
    if (!nome || nome == '' || typeof nome === 'undefined') {
        alert("Preencher o nome completo");
        $('#nome').focus();
        return false;
    }

    var tipo_pessoa = $('#tipo_pessoa').val();
    if (tipo_pessoa == "Juridica") {
        var cnpj = $('#cnpj').val();
        if (!cnpj || cnpj == '' || typeof cnpj === 'undefined') {
            alert("Preencher CNPJ válido do contratante");
            // $('#cnpj').focus();
            return false;
        }
    }

    if (tipo_pessoa == "Fisica") {
        var cpf = $('#cpf').val();
        if (!cpf || cpf == '' || typeof cpf === 'undefined' || !cpfValido(cpf)) {
            alert("Preencher CPF válido do contratante");
            $('#cpf').focus();
            return false;
        }


        var email = $('#email').val();
        if (!email || email == '' || typeof email === 'undefined') {
            alert("Preencher o E-mail");
            $('#email').focus();
            return false;
        }


        var telefone = $('#telefone').val();
        if (!telefone || telefone == '' || typeof telefone === 'undefined') {
            alert("Preencher o E-mail");
            $('#telefone').focus();
            return false;
        }

        var prazoMes = $('#prazoMes').val();
        if (!prazoMes || prazoMes == '' || typeof prazoMes === 'undefined') {
            alert("Preencher o prazo da obra ");
            $('#prazoMes').focus();
            return false;
        }

        var prazoDia = $('#prazoDia').val();
        if (!prazoDia || prazoDia == '' || typeof prazoDia === 'undefined') {
            alert("Preencher o prazo da obra ");
            $('#prazoDia').focus();
            return false;
        }

        var valor = $('#valor').val();
        if (!valor || valor == '' || typeof valor === 'undefined') {
            alert("Preencher o valor da obra ");
            $('#valor').focus();
            return false;
        }

        var endereco = $('#endereco').val();
        if (!endereco || endereco == '' || typeof endereco === 'undefined') {
            alert("Preencher o endereço completo ");
            $('#endereco').focus();
            return false;
        }

        var numero = $('#numero').val();
        if (!numero || numero == '' || typeof numero === 'undefined') {
            alert("Preencher o numero ");
            $('#numero').focus();
            return false;
        }

        var complemento = $('#complemento').val();
        if (!complemento || complemento == '' || typeof complemento === 'undefined') {
            alert("Preencher o complemento ");
            $('#complemento').focus();
            return false;
        }

        var cep = $('#cep').val();
        if (!cep || cep == '' || typeof cep === 'undefined') {
            alert("Preencher o cep ");
            $('#complemento').focus();
            return false;
        }

        var bairro = $('#bairro').val();
        if (!bairro || bairro == '' || typeof bairro === 'undefined') {
            alert("Preencher o bairro");
            $('#bairro').focus();
            return false;
        }

        var cidade = $('#cidade').val();
        if (!cidade || cidade == '' || typeof cidade === 'undefined') {
            alert("Preencher a cidade");
            $('#cidade').focus();
            return false;
        }

        var uf = $('#uf').val();
        if (!uf || uf == '' || typeof uf === 'undefined') {
            alert("Preencher o uf");
            $('#uf').focus();
            return false;
        }

        var reforma = $('#reforma').val();
        if (!reforma || uf == '' || typeof reforma === 'undefined') {
            alert("Selecionar o tipo de reforma");
            $('#reforma').focus();
            return false;
        }

        var obs = $('#obs').val();
        if (!obs || obs == '' || typeof obs === 'undefined') {
            alert("Preencher a descrição completa");
            $('#obs').focus();
            return false;
        }


        var estrutural = $('input[name=estrutural]:checked').val();
        if (!estrutural || estrutural == '' || typeof estrutural === 'undefined') {
            alert("Selecionar o tipo de estrutura ");
            $('#estrutural').focus();
            return false;
        }


        var demolicao = $('input[name=demolicao]:checked').val();
        if (!demolicao || demolicao == '' || typeof demolicao === 'undefined') {
            alert("Selecionar o tipo de demolição ");
            $('#demolicao').focus();
            return false;
        }

        var pavimentos = $('#pavimentos').val();
        if (!pavimentos || pavimentos == '' || typeof pavimentos === 'undefined') {
            alert("Selecionar o numero de pavimentos");
            $('#pavimentos').focus();
            return false;
        }

        var subsolos = $('#subsolos').val();
        if (!subsolos || subsolos == '' || typeof subsolos === 'undefined') {
            alert("Selecionar o numero de subsolos");
            $('#subsolos').focus();
            return false;
        }
    }

        return true;
    }

    function dataValida(date) {
        var matches = /^(\d{2})[-\/](\d{2})[-\/](\d{4})$/.exec(date);
        if (matches == null) return false;
        var d = matches[2];
        var m = matches[1] - 1;
        var y = matches[3];
        var composedDate = new Date(y, m, d);
        return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
    }

    function emailValido(email) {
        const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    }

    function cpfValido(c) {

        if ((c = c.replace(/[^\d]/g, '')).length != 11) {
            return false;
        }

        if (c == '00000000000') {
            return false;
        }

        if (c.length < 11) {
            return false;
        }

        if (/^(.)\1+$/.test(c)) {
            return false;
        }

        var r;
        var s = 0;
        var i = null;

        for (i = 1; i <= 9; i++) {
            s = s + parseInt(c[i - 1]) * (11 - i);
        }

        r = (s * 10) % 11;

        if ((r == 10) || (r == 11)) {
            r = 0;
        }

        if (r != parseInt(c[9])) {
            return false;
        }

        s = 0;

        for (i = 1; i <= 10; i++) {
            s = s + parseInt(c[i - 1]) * (12 - i);
        }

        r = (s * 10) % 11;

        if ((r == 10) || (r == 11)) {
            r = 0;
        }

        if (r != parseInt(c[10])) {
            return false;
        }

        return true;
    }


    function cotar() {
        if (!valida_form()) {
            return;
        }

        var nome = $('#nome').val() ? $('#nome').val().trim() : '';
        var tipo_pessoa = $('#tipo_pessoa').val() ? $('#tipo_pessoa').val().trim() : '';
        var cpf = $('#cpf').val() ? $('#cpf').val().trim() : '';
        var cnpj = $('#cnpj').val() ? $('#cnpj').val().trim() : '';
        var email = $('#email').val() ? $('#email').val().trim() : '';
        var telefone = $('#telefone').val() ? $('#telefone').val().trim() : '';
        var prazoMes = $('#prazoMes').val() ? $('#prazoMes').val().trim() : '';
        var prazoDia = $('#prazoDia').val() ? $('#prazoDia').val().trim() : '';
        var valor = $('#valor').val() ? $('#valor').val().trim() : '';
        var endereco = $('#endereco').val() ? $('#endereco').val().trim() : '';
        var numero = $('#numero').val() ? $('#numero').val().trim() : '';
        var complemento = $('#complemento').val() ? $('#complemento').val().trim() : '';
        var cep = $('#cep').val() ? $('#cep').val().trim() : '';
        var bairro = $('#bairro').val() ? $('#bairro').val().trim() : '';
        var cidade = $('#cidade').val() ? $('#cidade').val().trim() : '';
        var uf = $('#uf').val() ? $('#uf').val().trim() : '';
        var reforma = $('#reforma').val() ? $('#reforma').val().trim() : '';
        var obs = $('#obs').val() ? $('#obs').val().trim() : '';
        var estrutural = $('input[name=estrutural]:checked').val() ? $('input[name=estrutural]:checked').val().trim() : '';
        var demolicao = $('input[name=demolicao]:checked').val() ? $('input[name=demolicao]:checked').val().trim() : '';
        var pavimentos = $('#pavimentos').val() ? $('#pavimentos').val().trim() : '';
        var subsolos = $('#subsolos').val() ? $('#subsolos').val().trim() : '';
        var parceiro = getUrlParameter('pc') ? getUrlParameter('pc') : 'Globus';

        var retorno = {
            "nome": nome,
            "tipo_pessoa": tipo_pessoa,
            "cpf": cpf,
            "cnpj": cnpj,
            "email": email,
            "telefone": telefone,
            "prazoMes": prazoMes,
            "prazoDia": prazoDia,
            "valor": valor,
            "endereco": endereco,
            "numero": numero,
            "complemento": complemento,
            "cep": cep,
            "bairro": bairro,
            "cidade": cidade,
            "uf": uf,
            "reforma": reforma,
            "obs": obs,
            "estrutural": estrutural,
            "demolicao": demolicao,
            "pavimentos": pavimentos,
            "subsolos": subsolos,
            "parceiro": parceiro
        };

        var imageData = new FormData();

        imageData.append("json", JSON.stringify(retorno));


        console.log(JSON.stringify(retorno));
        $('#page_loader').removeClass('hide');
        $('#black_hole').addClass('hide');
        $('#footer').addClass('hide');

        $.ajax
        ({
            type: "POST",
            url: "engenharia_risco_cotacao.php",
            cache: false,
            contentType: false,
            processData: false,

            data: imageData,
            success: function (result) {
                $('#mensagem_sucesso').removeClass('hide');
                $('#footer').addClass('hide');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("Status: " + textStatus + " Error: " + errorThrown);
                alert("Ocorreu um erro ao processar a sua solicitação. Tente novamente, por favor.");
                $('#black_hole').removeClass('hide');
            },
            complete: function (data) {
                $('#page_loader').addClass('hide');
            }
        })

    }

