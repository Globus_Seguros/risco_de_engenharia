<?php
require_once('globals.php');
require_once('email_cotacao.php');
require_once('email_cotacao_cliente.php');
require_once('email_cotacao_parceiro.php');
//require_once('class/Mail.php');


header('Content-Type: application/json; charset=utf-8');
date_default_timezone_set('America/Sao_Paulo');

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $decoded = json_decode($_POST['json']);
    if (isset($decoded)) {
        if (is_null($decoded)) {
            $response['status'] = array(
                'type' => 'error',
                'value' => 'Invalid JSON value found',
            );
            $response['request'] = $_REQUEST['json'];
        } else {

//            $email = new Mail();
            /// cadastrar a cotacao
            $sqltimestamp = date('Y-m-d G:i:s');
            try {
                if ($GLOBALS['TESTE']) {
                    $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=rcmedicoTestdb user=RevoluTISys password=globusSeguros0");
                } else {
                    $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=globusdb user=RevoluTISys password=globusSeguros0");
                }

                $query = "INSERT INTO risco_engenharia (nome,tipo_pessoa,cpf, cnpj, email, telefone, prazo_mes, prazo_dia, valor, endereco, numero, complemento, cep, bairro, cidade, uf, reforma, obs, estrutural, demolicao, pavimentos, subsolos, parceiro, timestamp ) VALUES ('" . $decoded->{'nome'} . "','" . $decoded->{'tipo_pessoa'} . "','" . $decoded->{'cpf'} . "','" . $decoded->{'cnpj'} . "','" . $decoded->{'email'} . "','" .  $decoded->{'telefone'} . "','" . $decoded->{'prazoMes'} . "','" . $decoded->{'prazoDia'} . "','" . $decoded->{'valor'} . "','" .  $decoded->{'endereco'} . "','" . $decoded->{'numero'} . "','" . $decoded->{'complemento'} . "','" . $decoded->{'cep'} . "','" . $decoded->{'bairro'} . "','" . $decoded->{'cidade'} . "','" . $decoded->{'uf'} . "','" . $decoded->{'reforma'} . "','" . $decoded->{'obs'} . "','". $decoded->{'estrutural'} . "','" . $decoded->{'demolicao'} . "','" . $decoded->{'pavimentos'} . "','" . $decoded->{'subsolos'} . "','" .  $decoded->{'parceiro'} . "','" . $sqltimestamp . "') RETURNING risco_engenharia;";

                $result = pg_query($bdcon, $query);



                if (!$result || pg_affected_rows($result) < 1) {
                    $response['status'] = array(
                        'type' => 'error',
                        'value' => '40014 incorrect data.',
                    );
                    header('HTTP/1.0 400 Bad Request');
                    echo json_encode($response);
                    return;
                } else {


                    envia_email_para_cotacao($decoded);
                    envia_email_cotacao_cliente($decoded);

                }

                $response['status'] = array(
                    'type' => 'message',
                    'value' => 'Operation performed',
                );
                header('HTTP/1.0 200 OK');
                echo json_encode($response);
                return;


            } catch (Exception $e) {
                $response['status'] = array(
                    'type' => 'error',
                    'value' => 'could not connect',
                );
                $response['request'] = $decoded;
                error_log("Não conseguiu conectar com o banco de dados");
                echo json_encode($response);
                return;
            }
        }
    } else {
        $response['status'] = array(
            'type' => 'error',
            'value' => 'Invalid JSON value found.',
        );
        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode($response);
        return;
    }
}




