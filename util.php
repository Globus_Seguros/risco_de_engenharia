<?php
require_once('globals.php');


function email_gestor_comercial($parceiro)
{

    if (!$parceiro || $parceiro === null) {
        $parceiro = $GLOBALS['Parceiro_padrao'];
    }


    if ($GLOBALS['TESTE']) {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=rcmedicoTestdb user=RevoluTISys password=globusSeguros0");
    } else {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=globusdb user=RevoluTISys password=globusSeguros0");
    }

    if (!$bdcon) {
        throw new Exception('Não foi possível se conectar ao banco de dados. Arquivo ultil/email_gestor_comercial');
    }

    $query = "select A.cpf_comercial_responsavel, B.email_usuario from rede_parceiro as A
               left join sys_usuario as B ON A.cpf_comercial_responsavel = B.cpf_usuario
                where A.id_parceiro = '$parceiro';";

    $result = pg_query($bdcon, $query);

    if (!$result || pg_affected_rows($result) < 1) {
        $response['status'] = array(
            'type' => 'error',
            'value' => '40016 incorrect data.',
        );
        header('HTTP/1.0 400 Bad Request');
        echo json_encode($response);
        return;

    } else {
        $registro = pg_fetch_all($result);
    }

    return $registro;
}


function select_group ($priority, $destination){

    $query = "SELECT * FROM consorcio_grupos where destinacao='" . $destination . "' AND id_grupo='" . $priority . "';";

    if ($GLOBALS['TESTE']) {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=rcmedicoTestdb user=RevoluTISys password=globusSeguros0");
    } else {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=globusdb user=RevoluTISys password=globusSeguros0");
    }

    $result = pg_query($bdcon, $query);
    if (!$result ||  pg_num_rows($result) < 1) {
        return null;
    }
    $array = pg_fetch_all($result);
    if ($array == false) {
        return null;
    }
    return $array;
}

function get_group_by_card($group_array, $card){
    for($i=0; $i < count($group_array); $i++){
        if($group_array[$i]['nome_grupo'] == $card['nome_grupo'])
            return $group_array[$i];
    }
    return null;
}

function obtem_probabilidade($nome_grupo, $chance){
    if (!$nome_grupo || !$chance)
        return null;

    $query = "SELECT min_contemplacao FROM consorcio_grupo_historico where nome_grupo='" . $nome_grupo . "' order by min_contemplacao ASC";

    if ($GLOBALS['TESTE']) {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=rcmedicoTestdb user=RevoluTISys password=globusSeguros0");
    } else {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=globusdb user=RevoluTISys password=globusSeguros0");
    }
    $result = pg_query($bdcon, $query);
    $n_rows =  pg_num_rows($result);
    $array = pg_fetch_all($result);


    if (!$result ||  $n_rows < 1) {
        return 0;
    }
    $sumAll = 0;
    $bigger = 0.0;
    $smaller = 1.0;
    for ($i=0; $i < $n_rows; $i++) {
        $sumAll += $array[$i]['min_contemplacao'];
        if($array[$i]['min_contemplacao'] > $bigger)
            $bigger = $array[$i]['min_contemplacao'];
        if($array[$i]['min_contemplacao'] < $smaller)
            $smaller = $array[$i]['min_contemplacao'];
    }
    $avg = $sumAll / $n_rows;
    $n_above_avg = 0;
    $n_bellow_avg = 0;

    for ($i=0; $i < $n_rows; $i++) {
       if($array[$i]['min_contemplacao'] >= $avg)
           $n_above_avg++;
       else
           $n_bellow_avg++;
    }

    if($chance > $avg){
        if($chance > $bigger)
            return 1;
        $range = $bigger - $avg;
        $markup = $chance - $avg;
        $prob = ($markup * 0.49)/$range + 0.5;
        $correction_fator = $n_above_avg / $n_bellow_avg;

        if($correction_fator < 1)
            $prob = $prob * $correction_fator;
    }else{
        $bottom = 1 / $n_rows;
        if($chance < $smaller)
            return 0;
        $range =  $avg - $smaller;
        $markup = $chance - $smaller;
        $prob = ($markup * ($avg - $bottom))/$range + ($avg - $bottom);
        $correction_fator = $n_bellow_avg / $n_above_avg ;
        if($correction_fator < 1)
            $prob = $prob * $correction_fator;
    }

    return $prob;
}


function obtem_cartas ($cartas){
    if (!cartas)
        return null;

    $query = "SELECT * FROM consorcio_cartas where nome_grupo IN (" . $cartas . ") ORDER BY valor_carta ASC";

    if ($GLOBALS['TESTE']) {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=rcmedicoTestdb user=RevoluTISys password=globusSeguros0");
    } else {
        $bdcon = pg_connect("host=globusdb.coymbr1bkj3n.sa-east-1.rds.amazonaws.com port=5432 dbname=globusdb user=RevoluTISys password=globusSeguros0");
    }
    $result = pg_query($bdcon, $query);
    if (!$result ||  pg_num_rows($result) < 1) {
        return null;
    }
    $array = pg_fetch_all($result);
    if ($array == false) {
        return null;
    }

    return $array;

}
